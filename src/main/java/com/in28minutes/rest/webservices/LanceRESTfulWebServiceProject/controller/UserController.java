package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.controller;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.exception.UserNotFoundException;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.service.UserDaoService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//import org.springframework.hateoas.EntityModel;
//import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {// a better name would be UserController


    private UserDaoService userDaoService;

    @Autowired
    public UserController(UserDaoService userDaoService){
        this.userDaoService = userDaoService;
    }

    //GET /users
    @GetMapping("/users")
    public List<User> retrieveAllUsers(){
        return userDaoService.findAll();
    }

    //Get
    @GetMapping("/users/{id}")
    public User retrieveUser(@PathVariable String id){
        User user =  userDaoService.findOneUser(id);

        if(user == null){
            throw new UserNotFoundException("id - " + id); //throws an error pshhh obviously
        }

        return user;
    }

    @PutMapping("/users/{id}")
    public User updateUser(@Valid @RequestBody User updatedUser, @PathVariable String id) {

        updatedUser.setId(id);
        User user = userDaoService.updateUser(updatedUser, id);

        if (user == null) {
            throw new UserNotFoundException("id - " + id); //throws an error pshhh obviously
        }
        return user;
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable String id){
        userDaoService.deleteById(id);
    }


    @PostMapping("/users")
    public User createUser(@Valid @RequestBody User user){
        User savedUser = userDaoService.save(user);
        //CREATED
        // /user/{id}   savedUser.getId()

//        URI location = ServletUriComponentsBuilder
//                .fromCurrentRequest()
//                .path("/{id}")
//                .buildAndExpand(savedUser.getId())
//                .toUri();
//        //redirect : 303
//        return ResponseEntity.created(location).build(); //add location to response header
        //return created object with new id
        return savedUser;
    }

    //Retrieve all posts for a User - GET /users/{id}/posts
    @GetMapping("/users/{id}/posts")
    public List<Post> retrieveAllPostsFromUser(@PathVariable String id){
        return userDaoService.getPostsByUser(id);
    }

    //    Retrieve details of a post - GET /users/{id}/posts/{post_id}
    // "/posts/users/{userId}
    // "/posts (create, POST or PUT)
    //
    @GetMapping("/users/{userId}/posts/{postId}")
    public Post retrieveSpecificPostFromUser(@PathVariable String postId){
        return userDaoService.getPost(postId).get();
    }

    //    Create a posts for a User - POST /users/{id}/posts
    @PostMapping("/users/{id}/posts")
    public Post createPostForUser(@RequestBody Post post, @PathVariable String id){
        Post newPost = userDaoService.addPostToUser(post, id);
        //CREATED
        // /user/{id}   savedUser.getId()

        return newPost;

    }

//    Delete a post from a User - DELETE /users/{id}/posts/{post_id}

}
