package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

//@Entity
public class Post {

//    @Id
//    @GeneratedValue(generator = "system-uuid")
//    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String postID;

    private String postContent;

    private Date datePosted;

//    @JsonIgnore
//    @ManyToOne(fetch= FetchType.LAZY)
//    private User user;

    public Post() {
    }

    public Post(String postID, String postContent, Date datePosted) {
        this.postID = postID;
        this.postContent = postContent;
        this.datePosted = datePosted;
        //this.user = user;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//
//    @Override
//    public String toString() {
//        return "Post{" +
//                "postID='" + postID + '\'' +
//                ", postContent='" + postContent + '\'' +
//                ", datePosted=" + datePosted +
//                ", user=" + user +
//                '}';
//    }
}
