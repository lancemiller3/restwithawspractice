package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.service;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component //also add @Repository if it shall talk to database. Probably gonna be used in JPA part of course
public class UserDaoService {//DAO : Data Access Object

    private AWSRepository userRepository;
    //private PostJPARepository postRepository;

    @Autowired
    public UserDaoService(AWSRepository userRepository){
        this.userRepository = userRepository;
    }


    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User save(User user){

        if(user.getName() != null && user.getBirthDate() != null) {
           //user.setId(UUID.randomUUID().toString());
            userRepository.save(user);
        }
       return user;
    }

    public User findOneUser(String id){
        User user = userRepository.findById(id);
        return user;
    }

    public User updateUser(User updatedUser, String userId){
        if(updatedUser.getId() == null ){
            updatedUser.setId(userId);
        }
        else if(!updatedUser.getId().equals(userId)){
            return null;
        }
        userRepository.save(updatedUser);
        return updatedUser;
    }

    public void deleteById(String id){
        userRepository.deleteById(id);
    }

    public Post addPostToUser(Post post, String userId){
//        if(post.getUser() == null){
//            post.setUser(findOneUser(userId).get());
//        }
        //findOneUser(userId).get().addPost(post.getPostID());
        post.setPostID(UUID.randomUUID().toString());
        return userRepository.addPostToUser(post, userId);
        //return post;
    }

    public List<Post> getPostsByUser(String userId){
        return userRepository.getPostsForUser(userId);
    }

    public Optional<Post> getPost(String postId){
        return null;//postRepository.findById(postId);
    }
}
