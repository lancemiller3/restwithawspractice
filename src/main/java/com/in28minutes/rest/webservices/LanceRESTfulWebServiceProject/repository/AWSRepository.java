package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.File;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDeleteExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AWSRepository {

    private DynamoDBMapper dynamoMapper;
    private AmazonDynamoDB client;

    private static String TABLE_NAME = "Lance_Users";

    @Autowired
    public AWSRepository(DynamoDBMapper mapper, AmazonDynamoDB clientDB){
        this.dynamoMapper = mapper;
        this.client = clientDB;
            try {
                DescribeTableResult tableDescription = client.describeTable(TABLE_NAME);

            }
            catch (ResourceNotFoundException nf){
                CreateUsersDynamoTable();

            }
            catch (Exception ex){
                System.out.println(ex.toString());
            }

    }

    public List<User> findAll(){
        List<User> users = dynamoMapper.scan(User.class, new DynamoDBScanExpression());

        return users;
    }

    public User findById(String id){
//        User partitionValue = new User();
//        partitionValue.setId(id);
//
//        DynamoDBQueryExpression<User> queryExpression = new DynamoDBQueryExpression<User>()
//                .withHashKeyValues(partitionValue);
//        List<User> user = dynamoMapper.query(User.class, queryExpression);
//        return user.get(0);

        return dynamoMapper.load(User.class, id);
    }

    public User save(User user){
        dynamoMapper.save(user);// need to figure out a way to get the newly created user back with its new UUID id
        return user;//actually the user object is updated ith the new id BOOOOM
    }

    public void deleteById(String id){
        User partitionValue = new User();
        partitionValue.setId(id);

//        DynamoDBDeleteExpression<User> queryExpression = new DynamoDBDeleteExpression<User>()
//                .withHashKeyValues(partitionValue);

        dynamoMapper.delete(partitionValue);
        //return User;
    }

    public Post addPostToUser(Post post, String userId) {
        User user = findById(userId);

        List<Post> posts = user.getPosts();

        posts.add(post);

        user.setPosts(posts);

        dynamoMapper.save(user);

        return post;
    }

    public List<Post> getPostsForUser(String userID){
        return dynamoMapper.load(User.class, userID).getPosts();
    }




    public void CreateUsersDynamoTable() {



        DynamoDB dynamoDB = new DynamoDB(client);

        String tableName = TABLE_NAME;

        try {
            System.out.println("Attempting to create table; please wait...");
            Table table = dynamoDB.createTable(tableName,
                    Arrays.asList(new KeySchemaElement("UserID", KeyType.HASH)), // Partition key
                                                                                                //new KeySchemaElement("title", KeyType.RANGE)), // Sort key
                    Arrays.asList(new AttributeDefinition("UserID", ScalarAttributeType.S)),
                            //new AttributeDefinition("title", ScalarAttributeType.S)), //Probably Going to get rid of the sort key
                    new ProvisionedThroughput(10L, 10L));//maybe get rid of provisioned throughput?

            //CreateTableRequest table = new CreateTableRequest().setBil
            table.waitForActive();
            System.out.println("Success.  Table status: " + table.getDescription().getTableStatus());
            uploadSampleUserData();

        }
        catch (Exception e) {
            System.err.println("Unable to create table: ");
            System.err.println(e.getMessage());
        }

    }

    public void uploadSampleUserData() throws Exception {

        DynamoDB dynamoDB = new DynamoDB(client);

        Table table = dynamoDB.getTable(TABLE_NAME);


        ObjectMapper objectMapper = new ObjectMapper();

        List<User> userDataList = new ArrayList<User>();

        try {
            File userData = new File("usersdata.json");

            userDataList = objectMapper.readValue(userData, new TypeReference<List<User>>() {});
        }
        catch (Exception ex){
            System.out.println(ex.toString());
        }
        //objectMapper.readValue(new File("target/json_car.json"), Car.class);

        //JsonNode rootNode = new ObjectMapper().readTree(parser);
        //Iterator<JsonNode> iter = rootNode.iterator();


        ObjectNode currentNode;

        for (User user:userDataList) {
//            User item = new User();
//            item.setId(currentNode.path("id").asText());
//            item.setName(currentNode.path("name").asText());
//            item.setBirthDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(currentNode.path("birthDate").asText()));
//            item.setPosts(objectMapper.readValue(currentNode.path("posts").toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Post.class)));

            try {
                dynamoMapper.save(user);
                System.out.println("PutItem succeeded: " + user.getId());

            }
            catch (Exception e) {
                System.err.println("Unable to add user: " + user.getId());
                System.err.println(e.getMessage());
                break;
            }
        }
    }



}
