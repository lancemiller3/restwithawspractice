package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.Converter;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;

import java.io.IOException;
import java.util.List;

public class PostConverter implements DynamoDBTypeConverter<String, List<Post>> {

    @Override
    public String convert(List<Post> objects) {
        //Jackson object mapper
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String objectsString = objectMapper.writeValueAsString(objects);
            return objectsString;
        } catch (JsonProcessingException e) {
            //do something
        }
        return null;
    }

    @Override
    public List<Post> unconvert(String objectsString) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Post> objects = objectMapper.readValue(objectsString, new TypeReference<List<Post>>(){});
            return objects;
        } catch (JsonParseException e) {
            //do something
        } catch (JsonMappingException e) {
            //do something
        } catch (IOException e) {
            //do something
        }
        return null;
    }
}
