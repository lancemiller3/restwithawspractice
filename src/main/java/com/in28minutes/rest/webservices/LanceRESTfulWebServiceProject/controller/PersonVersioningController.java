package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.controller;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Name;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.PersonV1;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.PersonV2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersioningController {

    @GetMapping(value = "v1/person")
    public PersonV1 personV1(){
        return new PersonV1("Lance Miller");
    }

    @GetMapping(value = "v2/person")
    public PersonV2 personV2(){
        return new PersonV2(new Name("Lance", "Miller"));
    }
    //request parameter versioning
    //uri versioning
    @GetMapping(value = "/person/param", params = "version=1") //person/param?version=1
    public PersonV1 paramV1(){
        return new PersonV1("Lance Miller");
    }

    @GetMapping(value = "/person/param", params = "version=2") //person/param?version=2
    public PersonV2 paramV2(){
        return new PersonV2(new Name("Lance", "Miller"));
    }

    //header versioning
    @GetMapping(value = "/person/header", headers = "X-API-VERSION=1") // pass X-API-VERSION=1 in as Header Key-Value
    public PersonV1 headerV1(){
        return new PersonV1("Lance Miller");
    }

    @GetMapping(value = "/person/header", headers = "X-API-VERSION=2")
    public PersonV2 headerV2(){
        return new PersonV2(new Name("Lance", "Miller"));
    }

    //Media type versioning
    @GetMapping(value = "/person/produces", produces = "application/vnd.company.app-v1+json") // pass this as a value to header Key-Value as Accept as Key
    public PersonV1 producesV1(){
        return new PersonV1("Lance Miller");
    }

    @GetMapping(value = "/person/produces", produces = "application/vnd.company.app-v2+json")
    public PersonV2 producesV2(){
        return new PersonV2(new Name("Lance", "Miller"));
    }
}
