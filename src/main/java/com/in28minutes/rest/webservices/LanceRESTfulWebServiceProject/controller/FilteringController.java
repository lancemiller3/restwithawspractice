package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.controller;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.SomeBean;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class FilteringController {

    //only map field1 and field2
    @GetMapping("/filtering")
    public MappingJacksonValue retrieveSomeBean(){
        SomeBean someBean = new SomeBean("value1", "value2","value3");

        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("field1", "field2");
        FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);

        MappingJacksonValue mappingVar = new MappingJacksonValue(someBean);
        mappingVar.setFilters(filters);

        return mappingVar;
    }

    //only map field2 and field 3
    @GetMapping("/filtering-list")
    public MappingJacksonValue retrieveListOfSomeBean(){

        List<SomeBean> beanys =  Arrays.asList(new SomeBean("value1", "value2","value3"),
                new SomeBean("valudfe1", "valdfgue2","valdfgue3"),
                new SomeBean("valuewef1", "valuwere2","valuwere3"));

        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("field2", "field3");
        FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);

        MappingJacksonValue mappingVar = new MappingJacksonValue(beanys);
        mappingVar.setFilters(filters);

        return mappingVar;


    }
}
