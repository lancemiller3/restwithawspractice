package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanceResTfulWebServiceProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(LanceResTfulWebServiceProjectApplication.class, args);
	}

}
